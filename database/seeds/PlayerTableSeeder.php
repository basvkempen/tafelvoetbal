<?php

use Illuminate\Database\Seeder;

class PlayerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('players')->delete();

        $players = array(
            ['id' => 1, 'name' => 'Bas', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'name' => 'Mark', 'created_at' => new DateTime, 'updated_at' => new DateTime]
        );

        DB::table('players')->insert($players);
    }

}