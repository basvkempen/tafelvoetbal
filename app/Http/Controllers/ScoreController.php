<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ScoreController extends Controller {

    public function view() {
        $scores = \App\Score::orderBy('timestamp', 'desc')->get();
        $players = \App\Player::lists('name','id');
        return view('viewScores')->with(['scores'=>$scores, 'players'=>$players]);
    }

    public function enter()
    {
        $players = \App\Player::lists('name','id');
        return view('enterScore')->with('players', $players);
    }

    public function save() {
        $score = new \App\Score;
        $score->timestamp = $_POST['timestamp'];
        $score->player1_id = $_POST['player1_id'];
        $score->player2_id = $_POST['player2_id'];
        $score->player1_score = $_POST['player1_score'];
        $score->player2_score = $_POST['player2_score'];
        $score->save();
        return redirect('score');
    }

}
