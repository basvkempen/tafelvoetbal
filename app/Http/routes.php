<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/migrate', function()
{
	\Artisan::call('migrate');
	\Artisan::call('db:seed');
	//
});

Route::get('/', 'HomeController@index');
Route::get('/score', 'ScoreController@view');
Route::get('/score/enter', 'ScoreController@enter');
Route::post('/score/enter', 'ScoreController@save');

/*Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/