@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Vul een score in</div>
                    <div class="panel-body">
                        {{--<form action="score/save" method="post" name="enter_score">--}}
                            {!! Form::open() !!}
                            <div class="row">
                                <div class="col-md-5">
                                    <p class="text-left">
                                        <div class="form-group">
                                            {!! Form::select('player1_id', $players, null, array('class'=>'form-control')) !!} <br />
                                            {!! Form::text('player1_score', '', array('class'=>'form-control')) !!}
                                        </div>
                                    </p>
                                </div>
                                <div class="col-md-2">
                                    <p class="text-center">
                                        {!! Form::text('timestamp', date('d-m-Y'), array('class'=>'form-control datepicker')) !!}
                                    </p>
                                    <p class="text-center">VERSUS</p>
                                </div>
                                <div class="col-md-5 right">
                                    <p class="text-right">
                                        <div class="form-group">
                                        {!! Form::select('player2_id', $players, null, array('class'=>'form-control')) !!} <br />
                                            {!! Form::text('player2_score', '', array('class'=>'form-control')) !!}
                                        </div>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <p class="text-center">
                                        {!! Form::submit('Opslaan', array('class'=>'btn btn-success')) !!}
                                    </p>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
