@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @foreach($scores as $score)
                    <div class="row">
                        <div class="col-md-1"><p class="text-left">{{$score->player1_score}}</p></div>
                        <div class="col-md-3"><p class="text-right">{{$players[$score->player1_id]}}</p></div>
                        <div class="col-md-4"><p class="text-center">{{$score->timestamp}}<br />VERSUS</p></div>
                        <div class="col-md-3"><p class="text-left">{{$players[$score->player2_id]}}</p></div>
                        <div class="col-md-1"><p class="text-right">{{$score->player2_score}}</p></div>
                        <hr />
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection